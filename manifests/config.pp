class sshd::config {
  file { '/etc/ssh/sshd_config': # Config file of sshd service
    ensure => file,
    require => Package['openssh-server'], # Required package installation
    source => '/etc/puppet2/tppuppet/puppet_mod/sshd/files/sshd_config',
    notify => Service['sshd']
  }
}

