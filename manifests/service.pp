class sshd::service {
  service { 'sshd':
    name => $sshd::params::service_name,
    ensure => running, # service is running
    enable => true, # service is enable
    subscribe => File['/etc/ssh/sshd_config'],
    hasrestart => true, # restart the service after init script
  }
}
