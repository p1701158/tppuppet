class ssh_v2 {
  Class['sshd_v2::params']->Class['sshd_v2::install']->Class['sshd_v2::service']
  include sshd_v2::params, sshd_v2::install, sshd_v2::service
}

file { 'LocalSettings.php':
    path    => '/var/www/html/LocalSettings.php',
    ensure  => 'file',
    content => template('mediawiki/LocalSettings.erb'),
}
