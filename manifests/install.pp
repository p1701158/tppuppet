class sshd_v2::install ( $sshd_port = 23, $sshd_protocol = 2 ) {
  package { $sshd_v2::params::ssh_package_name:
	ensure => present,
	before => File['/etc/ssh/sshd_config'],
  }
  file { '/etc/ssh/sshd_config':
	ensure => file,
	mode => '0600',
	content => template("/etc/puppet2/tppuppet/puppet_mod/sshd_v2/templates/sshd_config.erb"),
  }
}
